// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace IntroBlazerSignalRChat.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using IntroBlazerSignalRChat.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\_Imports.razor"
using IntroBlazerSignalRChat.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\Pages\Index.razor"
using Microsoft.AspNetCore.SignalR.Client;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase, IDisposable
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 25 "C:\Users\vcaceres\source\repos\IntroBlazerSignalRChat\IntroBlazerSignalRChat\Client\Pages\Index.razor"
       
    private HubConnection hubConnection;
    private List<string> Messages = new List<string>();
    private string userInput;
    private string messageInput;

    protected override async Task OnInitializedAsync()
    {
        hubConnection = new HubConnectionBuilder()
            .WithUrl(NavigationManager.ToAbsoluteUri("/chathub")).Build();

        hubConnection.On<string, string>("ReceiveMessage", (user, message) => {
            var encodedMessage = $"{user}: {message}";
            Messages.Add(encodedMessage);
            StateHasChanged();
        });

        await hubConnection.StartAsync();
    }

    Task Send() => hubConnection.SendAsync("SendMessage", userInput, messageInput);

    public bool IsConnected => hubConnection.State == HubConnectionState.Connected;

    public void Dispose()
    {
        _ = hubConnection.DisposeAsync();
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
#pragma warning restore 1591
